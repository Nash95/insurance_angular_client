import { Component, OnInit } from '@angular/core';
import { Insurance } from '../models/insurance';
import { Vehicle} from '../models/vehicle'

import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';

//paynow
// var require: NodeRequire;
// const { Paynow } = require('paynow');
// import { Paynow } from 'paynow';




@Component({
  selector: 'app-insurance-type',
  templateUrl: './insurance-type.component.html',
  styleUrls: ['./insurance-type.component.css']
})
export class InsuranceTypeComponent implements OnInit {

  public insurance : Insurance = new Insurance();

  public vehicles : Vehicle = new Vehicle();

  public nash : any;
  public hey : any;
  public charge : any;

  comp : boolean = false;
  third : boolean = false;
  

  selectedType: string = '';


  //paynow

  // public paynow : Paynow = new Paynow();
  



  constructor(public apiService: ApiService , 
    public acRoute : ActivatedRoute) { }

  ngOnInit() {
  
    this.apiService.get("vehicles/1").subscribe((data : Vehicle)=>{
      console.log("Vehicle value = "+data);
      this.vehicles = data;
      this.hey = this.vehicles;

      // data.forEach(el=>{el.contact_num*=2});
      });
  }


  public onSubmit(){
    console.log("Adding a vehicle: " + this.insurance.insurance_type);
    alert();
   
  }

  premium(){

    // let nash: any;

    let duty;

    let disc;

    let prem : bigint;

    this.nash = this.hey*0.1;

    console.log("The final  rate is " + this.nash)

    // return this.nash;

    duty = this.nash*0.05;

    if (duty<2){
       duty = 2;
    }

    else if (duty>100){
      duty = 100;
    }

    else{
       duty;
    }


    console.log("The stampDuty is "+ duty)

    disc = this.nash*0.1;

    

    console.log("The discount is "+ disc)

    

    prem = this.nash - disc + duty;


    console.log("The premium is "+ prem)

    return prem;

  }

  compType(){
    this.comp = true;
    this.third= false;
    this.charge = this.premium()
    var handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
      locale: 'auto',
      token: function (token: any) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    


    handler.open({
      name: 'NASH Insurer',
      description: 'Please confirm payment',
      amount: this.charge*100
    });
  }
  thirdType(){
    this.third= true;
    this.comp = false;
    var handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
      locale: 'auto',
      token: function (token: any) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    handler.open({
      name: 'NASH Insurer',
      description: 'Please confirm payment',
      amount: 5000
    });
  }

  selectChangeHandler (event: any) {
    //update the ui
    this.selectedType = event.target.value;
    if ( this.selectedType=='compType'){
      this.compType()  
    }
    if ( this.selectedType=='thirdType'){
      this.thirdType()  
    }
  }

  getUrl()  {
    return "url('./assets/images/insurance5.jpg')";
  }



}

