import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../models/vehicle';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../models/client';
import { NgForm } from '@angular/forms'


@Component({
  selector: 'app-vehicle-add',
  templateUrl: './vehicle-add.component.html',
  styleUrls: ['./vehicle-add.component.css']
})
export class VehicleAddComponent implements OnInit {


  public vehicle : Vehicle = new Vehicle();
  public client : Client = new Client();
  submitted :boolean = false;

  constructor(public apiService: ApiService , public acRoute : ActivatedRoute) { }

  ngOnInit() {
    this.apiService.get("clients/1/").subscribe((data : Client)=>{
      this.client = data;
  
      console.log(data);
      // data.forEach(el=>{el.contact_num*=2});
  
      
      }) ;
  }

  public onSubmit(myForm : NgForm){
    this.submitted = true;
    console.log("Adding a vehicle: " + this.vehicle.make);

    this.vehicle.client_id = this.client;
    this.apiService.post("vehicles",this.vehicle).subscribe((r)=>{
    console.log(r);
    this.vehicle = new Vehicle();
    alert("vehicle added !");
    myForm.resetForm();

    // this.myRoute.navigate(['insurance']);

    });
  }
  


}
