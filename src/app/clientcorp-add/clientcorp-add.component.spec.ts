import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientcorpAddComponent } from './clientcorp-add.component';

describe('ClientcorpAddComponent', () => {
  let component: ClientcorpAddComponent;
  let fixture: ComponentFixture<ClientcorpAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientcorpAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientcorpAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
