import { Component, OnInit } from '@angular/core';
import { Client } from '../models/client';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from'@angular/forms'
import { AstMemoryEfficientTransformer } from '@angular/compiler';

@Component({
  selector: 'app-client-add',
  templateUrl: './clientcorp-add.component.html',
  styleUrls: ['./clientcorp-add.component.css']
})
export class ClientcorpAddComponent implements OnInit {

  constructor(public apiService: ApiService , public acRoute : ActivatedRoute) { }

  public client : Client = new Client();

  submitted : boolean = false;

  ngOnInit() {

    // this.acRoute.params.subscribe((data : any)=>{
    //   console.log(data.id);
    //   if(data && data.id){
    //       this.apiService.get("clients/"+data.id).subscribe((data : Client)=>{
    //       this.client = data;
    //       });
    //   }
    //   else
    //   {
    //       this.client = new Client();
    //   }
    //   })

  }

  public onSubmit(myForm: NgForm){
    this.submitted = true;
    console.log("Adding a client: " + this.client.client_name);
    // if(this.client.client_id){
    // this.apiService.update("clients/"+this.client.client_id,this.client).subscribe((r)=>{
    //     console.log(r);
    //     alert("client updated !");
    // })
    // }
    // else
    this.apiService.post("clients",this.client).subscribe((r)=>{
    console.log(r);
    this.client = new Client();
    alert("client added !");

    myForm.resetForm();
  
    });
  }



}
