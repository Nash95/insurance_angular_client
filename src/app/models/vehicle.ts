export class Vehicle{
    public engine_number:number;
    public make:string;
    public model:string;
    public manufacture_year:number;
    public chasis_number:number;
    public cubic_capacity:number;
    public vehicle_value:any;
    public client_id:any;
}   