export class Client{
    public client_id:number;
    public client_name:string;
    public client_surname:string;
    public contact_num:number;
    public email_address:string;
    public address:string;
}