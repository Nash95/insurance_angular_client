import { Component, OnInit } from '@angular/core';
import { Client } from '../models/client';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Router } from '@angular/router';

@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.css']
})
export class ClientAddComponent implements OnInit {

  constructor(public apiService: ApiService , public acRoute : ActivatedRoute, public router : Router ) { }

  public client : Client = new Client();

  public clients : Array<Client>;

  submitted : boolean = false;

  ngOnInit() {      

  }

  public onSubmit(myForm: NgForm){
    this.submitted = true;
    console.log("Adding a client: " + this.client.client_name);
    // if(this.client.client_id){
    // this.apiService.update("clients/"+this.client.client_id,this.client).subscribe((r)=>{
    //     console.log(r);
    //     alert("client updated !");
    // })
    // }
    // else
    this.apiService.post("clients",this.client).subscribe((r)=>{
    console.log(r);
    this.client = new Client();
    alert("client added !");
    myForm.resetForm();

    });
  }

  





}
