import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component'
import { ClientAddComponent } from './client-add/client-add.component';
import { ClientcorpAddComponent } from './clientcorp-add/clientcorp-add.component';
import { VehicleAddComponent } from './vehicle-add/vehicle-add.component'
import { InsuranceTypeComponent } from './insurance-type/insurance-type.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component:HomeComponent },
  { path: 'clients', component: ClientAddComponent },
  { path: 'clientcorps', component: ClientcorpAddComponent },
  { path: 'vehicles', component: VehicleAddComponent },
  { path: 'insurance', component: InsuranceTypeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
