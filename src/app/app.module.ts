import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ApiService } from './api.service';
import { HomeComponent } from './home/home.component';
import { ClientAddComponent } from './client-add/client-add.component';
import { VehicleAddComponent } from './vehicle-add/vehicle-add.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ClientcorpAddComponent } from './clientcorp-add/clientcorp-add.component';
import { InsuranceTypeComponent } from './insurance-type/insurance-type.component';



@NgModule({
  declarations: [
    AppComponent,
    ClientAddComponent,
    VehicleAddComponent,
    HomeComponent,
    ClientcorpAddComponent,
    InsuranceTypeComponent
  ],
  imports: [    
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    MDBBootstrapModule.forRoot()
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
