# insurance_angular_client

## Steps to configure the app for use
1. Make sure you have angular CLI framework installed on your local machine first, if not please follow the following link or any other for steps to install: https://cli.angular.io/ .
2. Then clone the repositories(including the insurance_core backend) onto your local machine.
3. The client works with a rails restful API hence the *insurance_core* should be cloned, configured and ran first.
4. Open terminal/gitbash from your local machine in the project's directory and run npm install to pull dependancies for the project.
5. Start the server.
6. After running the application, open your browser navigate to the url "http://localhost:4200/" to start insurance processing.

## Processing insurance
1. The user begins by selecting whether he/she is an individual or coporate client.
2. The system then captures the client details.
3. After capture the client navigates to the vehicle details capture form and fills his/her vehicle details.
4.  The system evaluates two types of insurance namely *Third Part* and *Comprehensive* insurance.
5. The client is then prompted to select the desired insurance type for processing.
6. If *Third Part* insurance is selected, the system processes the payment based on a fixed amount, say $50 and prompts the user for his/her credit card credentials to initialise payment via a stripe payment gateway.
7. Else if the user select *Comprehensive* insurance, the system processes insurance and calculates the amount based on the captured vehicle value generated from the following principles :

- Premium rate (10% of the vehicle value)
- Stamp Duty (5% of the premium rate with a minimum of $2 and a maximum of $100)
- Discount (10% of the premium rate)

**Overal Comprehensive Insurance = Premium rate + Stamp duty - Discount**

The client is prompted for his/her credit card credentials and the insurance is processed.

8. If the client saves his/her credit card for future use, *stripe* sends a confirmation text message to the user's mobile number. 




